<?php

use Evtefeev\Framework\Route;
use Symfony\Component\Dotenv\Dotenv;

require_once '../vendor/autoload.php';
require_once '../packages/evtefeev/framework/src/functions.php';

define('URL', trim($_SERVER['REQUEST_URI'], ''));
define('ROOT', dirname(__DIR__));

session_start();

$dotenv = new Dotenv();
$dotenv->load(ROOT . '/.env');

require_once ROOT . '/routes/web.php';

Route::dispatch(URL);
