<?php

namespace App\Models;

use Evtefeev\Framework\Models\Model;
use PDOException;

/**
 * class UserModel
 *
 * @author Nikita Evtefeev <nikita.evtefeev@gmail.com>
 */
class UserModel extends Model
{
    protected string $table = 'users';
    protected bool $created_at = true;
    protected bool $updated_at = true;

    protected array $tableColumns = ['id', 'email', 'first_name', 'last_name', 'password', 'created_at', 'updated_at'];
    protected array $showColumns = ['id', 'email', 'first_name', 'last_name', 'created_at', 'updated_at', 'gender_id'];

    /**
     * @param int $id
     * @param bool $show_gender
     * @return array
     *
     */
    public function getOne(int $id, bool $show_gender = true): array
    {
        $user = $this->select($this->table, $this->showColumns, 'id', $id)[0];
        if ($show_gender) {
            $gender = $this->getGender($id);
            unset($user['gender_id']);
            $user['gender'] = $gender;
        }
        return $user;
    }

    /**
     * @param string $id
     * @return string
     */
    public function getGender(string $id): string
    {
        $gender_id = $this->selectVal($this->table, 'gender_id', 'id', $id);
        return $this->selectVal('gender', 'value', 'id', $gender_id);
    }

    public function create(array $data): int
    {
        try {
            return $this->insert($data);
        } catch (PDOException) {
            return -1;
        }

    }

    /**
     * @param array $credentials
     * @return int
     */
    public function checkLogin(array $credentials): int
    {
        $email = $credentials['email'];
        $pass = $credentials['password'];
        $hash = $this->selectVal($this->table, 'password', 'email', $email);
        if (password_verify($pass, $hash)) {
            return (int)$this->selectVal($this->table, 'id', 'email', $email);
        } else {
            return -1;
        }
    }

    /**
     * @param int $user_id
     * @return bool
     */
    public function remove(int $user_id): bool
    {
        $this->delete('id', $user_id);
        return true;
    }

    /**
     * @param array $data
     * @return bool
     */
    public function edit(array $data): bool
    {
        $this->update('id', $_SESSION['user_id'], $data);
        return true;
    }
}