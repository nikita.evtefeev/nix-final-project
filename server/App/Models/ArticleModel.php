<?php

namespace App\Models;

use Evtefeev\Framework\Models\Model;

/**
 * class ArticleMode
 *
 * @author Nikita Evtefeev <nikita.evtefeev@gmail.com>
 */
class ArticleModel extends Model
{
    protected string $table = 'articles';
    protected bool $created_at = true;
    protected bool $updated_at = true;
    protected array $tableColumns = ['id', 'title', 'description', 'tag', 'created_at', 'updated_at', 'user_id', 'tag_id'];
    protected array $showColumns = ['id', 'title', 'description', 'tag', 'created_at', 'updated_at', 'user_id'];

    /**
     * @param string $user_id
     * @param string $tag
     * @return array
     */
    public function getAll(string $user_id = '', string $tag = ''): array
    {
        if ($user_id == '') {
            if ($tag == '') {
                return $this->select($this->table, $this->showColumns, 1, 1, '-created_at');
            } else {
                return $this->select($this->table, $this->showColumns, 'tag', $tag, '-created_at');
            }
        } else {
            return $this->select($this->table, $this->showColumns, 'user_id', $user_id, '-created_at');
        }
    }

    /**
     * @param string $id
     * @return array
     */
    public function getOne(string $id): array
    {
        $res = $this->select($this->table, $this->showColumns, 'id', $id);
        if (count($res) == 1) {
            return $res[0];
        }
        return $res;
    }

    /**
     * @param array $data
     * @return int
     */
    public function create(array $data): int
    {
        $data['user_id'] = $_SESSION['user_id'];
        $this->addTag($data);
//        dump($data);
        return $this->insert($data);
    }

    /**
     * @param $id
     * @return array
     */
    public function getAuthor($id): array
    {
        return $this->select('users', ['id', 'email'], 'id', $id)[0];
    }

    /**
     * @param $id
     * @param $data
     * @return void
     */
    public function edit($id, $data): void
    {
        $this->addTag($data);
        $this->update('id', $id, $data);
    }


    /**
     * @param $id
     * @return void
     */
    public function remove($id): void
    {
        $this->delete('id', $id);
    }

    /**
     * @param array $data
     * @return void
     */
    private function addTag(array &$data): void
    {
        if (isset($data['tag'])) {
            $tagId = $this->selectVal('tags', 'id', 'name', $data['tag']);
            if ($tagId == '') {
                $tagId = $this->insert(['name' => $data['tag']], 'tags');
            }
            $data['tag_id'] = $tagId;
        }
    }

    /**
     * @return array
     */
    public function getTags(): array
    {
        $tags = $this->select('tags', 'name', 1, 1);
        return array_map(function ($obj) {
            return $obj['name'];
        }, $tags);
    }
}