<?php

namespace App\Controllers;

use Evtefeev\Framework\Controllers\Controller;

/**
 * Class BaseController
 *
 * @author Nikita Evtefeev <nikita.evtefeev@gmail.com>
 */
class BaseController extends Controller
{

}