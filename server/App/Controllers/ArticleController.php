<?php

namespace App\Controllers;

use App\Models\ArticleModel;

/**
 * Class ArticleController
 *
 * @author Nikita Evtefeev <nikita.evtefeev@gmail.com>
 */
class ArticleController extends BaseController
{

    /**
     * @var ArticleModel
     */
    protected ArticleModel $model;


    /**
     * @param $route
     */
    public function __construct($route)
    {
        parent::__construct($route);
        $this->model = new ArticleModel();
    }

    /**
     * @param array $data
     * @return bool
     */
    public function index(array $data): bool
    {
//        dumpd('wtf');
        $tags = $this->model->getTags();

        if (isset($data['tag'])) {
            $tag = $data['tag'];
            $articles = $this->model->getAll('', $tag);
        } else {
            $articles = $this->model->getAll();
        }

        return $this->view('index', compact(['articles', 'tags']));
    }

    /**
     * @param array $params
     * @return bool
     */
    public function showArticle(array $params): bool
    {
        $id = $params['id'];
        $data = $this->model->getOne($id);
        $title = $data['title'];
        $author = $this->model->getAuthor($data['user_id']);
        return $this->view('article/index', compact(['data', 'title', 'author']));
    }

    /**
     * @return bool
     */
    public function createArticle(): bool
    {
        if ($_POST) {
            $data = $_POST;
            $id = $this->model->create($data);
            $this->redirect('article/id/' . $id);
        }
        if (isset($_SESSION['user_id'])) {
            return $this->view('article/create', ['title' => 'Create Article']);
        } else {
            $this->redirect('login');
        }
        return true;
    }

    /**
     * @param array $params
     * @return bool
     */
    public function showUserArticles(array $params): bool
    {
        $id = $params['id'];
        $articles = $this->model->getAll($id);
        return $this->view('index', ['title' => 'User articles', 'articles' => $articles]);
    }

    /**
     * @param $params
     * @return bool
     */
    public function updateArticle($params): bool
    {
        $id = $params['id'];
        if ($_POST) {
            $this->model->edit($id, $_POST);
            $this->redirect('article/id/' . $id);
        }
        $data = $this->model->getOne($id);
        return $this->view('article/update', ['title' => 'Edit article', 'data' => $data]);
    }

    /**
     * @param $params
     * @return bool
     */
    public function deleteArticle($params): bool
    {
        $id = $params['id'];
        $this->model->remove($id);
        return $this->view('article/delete', ['title' => 'Delete article', 'message' => 'Successfully removed']);
    }
}