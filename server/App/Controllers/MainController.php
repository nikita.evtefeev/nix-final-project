<?php

namespace App\Controllers;

use App\Models\ArticleModel;

/**
 * class MainController
 *
 * @author Nikita Evtefeev <nikita.evtefeev@gmail.com>
 */
class MainController extends BaseController
{
    /**
     * @return bool
     */
    public function index(): bool
    {
        $articleModel = new ArticleModel();
        $articles = $articleModel->getAll();
        $tags = $articleModel->getTags();
        $title = 'Last created articles';
        return $this->view('/index', compact(['articles', 'tags', 'title']));

    }
}