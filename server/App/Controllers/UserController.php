<?php

namespace App\Controllers;

use App\Models\UserModel;
use PDOException;

/**
 * class UserController
 *
 * @author Nikita Evtefeev <nikita.evtefeev@gmail.com>
 */
class UserController extends BaseController
{
    /**
     * @var UserModel
     */
    protected UserModel $model;

    /**
     * @param $route [class self, function method]
     */
    public function __construct($route)
    {
        parent::__construct($route);
        $this->model = new UserModel();
    }

    /**
     * @return bool
     */
    public function index(): bool
    {
        if (isset($_SESSION['user_id'])) {
            $user = $this->model->getOne($_SESSION['user_id']);
            $title = 'Cabinet';
            return $this->view('/user/index', compact(['user', 'title']));
        } else {
            return $this->view('/user/login');
        }
    }

    /**
     * @param string $message
     * @return bool
     */
    public function register(string $message = ''): bool
    {
        $title = 'register';
        $text = $message;
        $data = ['title', 'text'];
        return $this->view('/user/register', compact($data));
    }

    public function create(): void
    {
        $data = $_POST;
        if ($data['password'] == $data['repeat-password']) {
            unset($data['repeat-password']);
            $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
            $id = $this->model->create($data);
            if ($id != -1) {
                $_SESSION['user_id'] = $id;
                $this->index();
            } else {
                $this->register('Email already exist');
            }
        } else {
            $this->register('Passwords not match');
        }
    }

    /**
     * @return bool
     */
    public function update(): bool
    {
        if ($_SESSION['user_id']) {
            if ($_POST) {
                $data = $_POST;
                if (isset($data['password'])) {
                    if ($data['password'] == $data['repeat-password']) {
                        unset($data['repeat-password']);
                        $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
                    } else {
                        $user = $this->model->getOne($_SESSION['user_id'], false);
                        return $this->view('/user/update', ['title' => 'Edit user info', 'user' => $user, 'text' => 'Passwords not match']);
                    }
                } else {
                    unset($data['repeat-password']);
                    unset($data['password']);
                }
                try {
                    $this->model->edit($data);
                    $this->redirect('user');
                } catch (PDOException) {
                    $user = $this->model->getOne($_SESSION['user_id'], false);
                    return $this->view('/user/update', ['title' => 'Edit user info', 'user' => $user, 'text' => 'Email already exist']);
                }
            }
            $user = $this->model->getOne($_SESSION['user_id'], false);
            return $this->view('/user/update', ['title' => 'Edit user info', 'user' => $user]);
        } else {
            $this->redirect('login');
            return false;
        }
    }

    /**
     * @return bool
     */
    public function delete(): bool
    {
        if (isset($_SESSION['user_id'])) {
            $this->model->remove($_SESSION['user_id']);
            unset($_SESSION['user_id']);
            return $this->view('/user/delete', ['message' => 'Successfully deleted']);
        } else {
            return $this->view('/user/delete', ['messageEr' => 'User not found']);
        }

    }

    public function logout()
    {
        unset($_SESSION['user_id']);
        $this->redirect('login');
    }

    /**
     * @return bool
     */
    public function login(): bool
    {
        if ($_POST) {
            $id = $this->model->checkLogin($_POST);
            if ($id != -1) {
                $_SESSION['user_id'] = $id;
                $this->redirect('user');
            } else {
                return $this->view('/user/login', ['title' => 'Login', 'error' => 'Invalid email or password']);
            }
        }
        return $this->view('/user/login', ['title' => 'Login']);
    }
}