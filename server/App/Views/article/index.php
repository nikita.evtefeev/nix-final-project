<!--<h1>Article view</h1>-->

<?php

if(empty($data) || empty($author)){
    echo 'Article not found';
    die();
}

?>


<h1>
    <?= $title ?? '' ?>
</h1>
<p>
    <?= $data['description'] ?? '' ?>
</p>
Date: <?= $data['updated_at'] ?? '' ?>
<br>
Tag: <i><?= $data['tag'] ?? '' ?></i>
<br>
Author: <a href="/user/articles/<?= $author['id'] ?? '' ?>"><?= $author['email'] ?? '' ?></a>
<br>
<?php if ($author['id'] == @$_SESSION['user_id']): ?>
    <br>
    <a type="button" class="btn btn-secondary" href="/article/update/<?= $data['id'] ?>">Edit article</a>

    <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#exampleModal">Delete</button>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Confirm</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    Remove this article?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
                    <a type="button" class="btn btn-danger" href="/article/delete/<?= $data['id'] ?>">Yes</a>
                </div>
            </div>
        </div>
    </div>


<?php endif?>