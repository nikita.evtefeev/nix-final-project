<?php

if (isset($message)) {
    echo '<div class="alert alert-success" role="alert">
' . $message . '</div>';
}

if (isset($messageEr)) {
    echo '<div class="alert alert-danger" role="alert">
' . $messageEr . '</div>';
}