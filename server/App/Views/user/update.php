<!--<form action="/user/update" method="post">-->
<!--    <label>-->
<!--        Email: <br>-->
<!--        <input type="email" name="email">-->
<!--    </label><br>-->
<!--    <label>-->
<!--        Password: <br>-->
<!--        <input type="password" name="password">-->
<!--    </label><br>-->
<!--    <input type="radio" id="male" name="gender_id" value="1" checked="checked">-->
<!--    <label for="male">Male</label><br>-->
<!--    <input type="radio" id="female" name="gender_id" value="2">-->
<!--    <label for="female">Female</label><br>-->
<!---->
<!--    <br>-->
<!--    <button type="submit">Send</button>-->
<!--</form>-->
<!---->
<!---->
<!--<h1>Register</h1>-->
<?php
if (isset($text) && $text != '') {
    echo '<div class="alert alert-warning" role="alert">' . $text . '</div>';
}
if (empty($user)) {
    echo 'user not found';
    die();
}
//dump($user);
?>


<form action="/user/update" method="post">
    <div class="col-xs-6 col-sm-5 col-md-4">
        <div class="mb-3 ">
            <label for="first_name" class="form-label">First name:</label>
            <input type="text" name="first_name" class="form-control" id="first_name"
                   value="<?= $user['first_name'] ?>">
        </div>
        <div class="mb-3 ">
            <label for="last_name" class="form-label">Last name:</label>
            <input type="text" name="last_name" class="form-control" id="last_name" value="<?= $user['last_name'] ?>">
        </div>
        <div class="mb-3 ">
            <label for="email" class="form-label">Email:</label>
            <input type="email" name="email" class="form-control" id="email" value="<?= $user['email'] ?>">
        </div>
        <div class="mb-3">
            <label for="password" class="form-label">Password:</label>
            <input type="password" name="password" class="form-control" id="password">
        </div>
        <div class="mb-3 ">
            <label for="repeat-password" class="form-label">Repeat password:</label>
            <input type="password" name="repeat-password" class="form-control" id="repeat-password">
        </div>
        <div class="mb-3">
            <label for="gender" class="form-label">Gender:</label>
            <select id="gender" name="gender_id" class="form-select" aria-label="Gender:">
                <option value="0" <?= $user['gender_id'] == 0 ? 'selected' : '' ?>>Not known</option>
                <option value="1" <?= $user['gender_id'] == 1 ? 'selected' : '' ?>>Male</option>
                <option value="2" <?= $user['gender_id'] == 2 ? 'selected' : '' ?>>Female</option>
                <option value="9" <?= $user['gender_id'] == 9 ? 'selected' : '' ?>>Not applicable</option>
            </select>
        </div>
        <div class="mb3">
            <button type="submit" class="btn btn-success">Send</button>

        </div>
    </div>
</form>
