<h2><?= $title ?? 'Articles' ?></h2>
<?php
if (isset($tags)):
    ?>


    <nav class="navbar navbar-expand-lg bg-light m-10">
        <div class="container-fluid">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="/article">ALL</a>
                </li>
                <?php
                foreach ($tags as $tag) {
                    echo '<li class="nav-item">
        <a class="nav-link" href="/article/tags/' . $tag . '">' . strtoupper($tag) . '</a>
    </li>';

                }

                //                dump($tags);
                ?>


            </ul>
        </div>
    </nav>
<?php endif ?>
<br>

<?php
if (!empty($articles)) {
//        dumpd($articles);
    foreach ($articles as $article) {

        echo '<h3><a href="/article/id/' . $article['id'] . '">' . $article['title'] . '</a></h3>';
        echo '<p>' . $article['description'] . '</p>';
    }
} else {
    echo 'Articles not found';
}
?>
