<?php

namespace Evtefeev\Framework;

/**
 * class Route
 *
 * @author Nikita Evtefeev <nikita.evtefeev@gmail.com>
 */
class Route
{
    /**
     * @var array all routes patterns like [string url => [class, method]]
     */
    protected static array $routes = [];

    /**
     * @var array route for current page [class, method]
     */
    protected static array $route = [];

    /**
     * @var array url params like [param => value]
     */
    protected static array $params = [];

    /**
     * @param string $url
     * @param array $route [class class, function method]
     * @return void
     */
    public static function add(string $url, array $route = []): void
    {
        self::$routes[$url] = $route;
    }

    /**
     * @param string $url
     * @return void
     */
    public static function dispatch(string $url): void
    {
        if (self::matchRoute($url)) {
            $controller = self::$route[0];
            if (class_exists($controller)) {
                $obj = new $controller(self::$route);
                $action = self::$route[1];
                if (method_exists($obj, $action)) {
                    if (!empty(self::$params)) {
                        $obj->$action(self::$params);
                    } else {
                        $obj->$action();
                    }
                } else {
                    echo 'Method "' . $action . '" in  ' . get_class($obj) . ' not exist';
                }
            } else {
                echo 'Controller' . $controller . ' not found';
            }
        } else {
            http_response_code(404);
            require '404.php';
        }
    }

    /**
     * @param string $url
     * @return bool
     */
    public static function matchRoute(string $url): bool
    {
        foreach (self::$routes as $pattern => $route) {
            if (str_contains($pattern, '$')) {
                [$pattern_url, $param] = explode('$', $pattern);
                $url_slices = explode('/', $url);
                $value = array_pop($url_slices);
                $right_url = implode('/', $url_slices).'/';
                self::$params[$param] = $value;
            } else {
                $pattern_url = $pattern;
                $right_url = $url;
            }
            if ($pattern_url == $right_url) {
                self::$route = $route;
                return true;
            }
        }
        return false;
    }
}


