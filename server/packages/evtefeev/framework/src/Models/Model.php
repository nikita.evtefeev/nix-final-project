<?php

namespace Evtefeev\Framework\Models;

use Evtefeev\Framework\DB;

/**
 * class Model
 *
 * @author Nikita Evtefeev <nikita.evtefeev@gmail.com>
 */
class Model
{
    private static DB $pdo;
    protected string $table = '';
    protected bool $created_at = false;
    protected bool $updated_at = false;
    protected array $tableColumns = [];

    public function __construct()
    {
        self::$pdo = DB::instance();
    }

    /**
     * @param string $table table
     * @param array|string $columns columns
     * @param string $param param or params
     * @param string $value value
     * @param string $order
     * @return array
     */
    protected function select(string $table, array|string $columns, string $param, string $value, string $order = ''): array
    {
        if (is_array($columns)) {
            $columns = implode(', ', $columns);
        }
        $orders = '';
        if ($order != '') {
            $ask = $order[0] == '-' ? ' DESC' : ' ASC';
            $order = ltrim($order, '-');
            $orders = " ORDER BY $order $ask";
        }
        $sql = "SELECT " . $columns . " FROM $table WHERE $param = '$value'" . $orders;
        return self::$pdo->query($sql);
    }

    /**
     * @param string $table table
     * @param string $column column
     * @param string $param param
     * @param string $value value
     * @return string
     */
    protected function selectVal(string $table, string $column, string $param, string $value): string
    {
        return $this->select($table, $column, $param, $value)[0][$column] ?? '';
    }

    /**
     * @param array $params
     * @param string $table
     * @return int
     */
    protected function insert(array $params, string $table = ''): int
    {
        $array = $this->escape($params);
//        dump($array);
        if ($table == '') {
            $table = $this->table;
            if ($this->created_at && $this->updated_at) {
                $array['created_at'] = date('Y-m-d H:i:s');
                $array['updated_at'] = date('Y-m-d H:i:s');
            }
        }
        $keyString = implode(', ', array_keys($array));
        $valuesString = implode('\', \'', array_values($array));
        $sql = "INSERT INTO " . $table . " ($keyString) VALUES ('$valuesString')";
//        dumpd($sql);
        return self::$pdo->queryId($sql);
    }

    /**
     * @param string $column
     * @param string $value
     * @return array array
     */
    protected function delete(string $column, string $value): array
    {
        $sql = "DELETE FROM $this->table WHERE $column = $value";
        return self::$pdo->query($sql);
    }

    /**
     * @param string $search_column
     * @param string $search_value
     * @param array $params
     * @return array
     */
    protected function update(string $search_column, string $search_value, array $params): array
    {
        if ($this->updated_at) {
            $params['updated_at'] = date('Y-m-d H:i:s');
        }
        $array = $this->escape($params);

        $set = '';
        foreach ($array as $index => $param) {
            $set .= "$index = '$param', ";
        }
        $set = rtrim($set, ', ');

        $sql = "
        UPDATE $this->table
        SET $set
        WHERE $search_column = $search_value;
        ";
        return self::$pdo->query($sql);
    }

    /**
     * @param $params
     * @return array
     */
    private function escape($params): array
    {
        return array_map(function ($val) {
            $val = str_replace("'", "''", $val);
            return htmlspecialchars($val);
        }, $params);
    }
}