<?php

namespace Evtefeev\Framework\Controllers;

use Evtefeev\Framework\Views\View;

/**
 * Class Controller.
 *
 * @author Nikita Evtefeev <nikita.evtefeev@gmail.com>
 */
abstract class Controller
{
    /**
     * @var array [class Controller, string method]
     */
    public array $route = [];

    /**
     * @param $route
     */
    public function __construct($route)
    {
        $this->route = $route;
    }

    /**
     * @param string $viewPath
     * @param array $data
     * @return bool
     */
    public function view(string $viewPath, array $data = []): bool
    {
        $viewObject = new View($this->route, $viewPath);
        $viewObject->render($data);
        return true;
    }

    public function redirect($path): void
    {
        $host = $_SERVER['HTTP_HOST'];
        $header = 'Location: http://' . $host . '/' . $path;
        header($header);
    }
}